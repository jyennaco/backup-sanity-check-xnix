class Logger

	def self.output_major_status(message="Processing")
		puts "-" * 60
		puts "#{message}".center(60)
		puts "-" * 60
	end

	def self.output_minor_status(message="Processing", type=nil)
		print "  #{message}"
		puts "." if type == :complete
		puts "" if type == :none
		puts "..." if type == nil
	end
	
	def self.output_error(message="Error")
		puts("\n  ERROR===> #{message} <===ERROR\n")
	end
	
	def self.output_user_info(message="Info")
		puts("\t#{message}")
	end
	
	def self.get_user_input(message="Input")
		print("\t#{message}: ")
	end
	
	def self.output_user_alert(message="Alert")
		puts("\t===> #{message} <===")
	end
	
	def self.info(message="info")
		puts(Time.now.to_s + " [INFO] " + message)
	end
	
	def self.warn(message="warn")
		puts(Time.now.to_s + " [WARN] " + message)
	end

	def self.error(message="error")
		puts(Time.now.to_s + " [ERROR] " + message)
	end
	
	def self.debug(message="debug")
		puts(Time.now.to_s + " [DEBUG] " + message)
	end
end
