# compare.rb
# 
# Joseph Yennaco, 31 March 2014
#

require 'fileutils'
require 'timeout'

class Compare
	
	# Get the user's home directory for reading and outputting problem files
	@@home_dir = File.join(File.expand_path('~'))
	
	@@source_path = "done"
	@@target_path = "done"
	
	# Set the max time to wait before interrupting a file copy
	@@max_time = 3600
	
	# Initialize the empty skip_items array
	@@skip_items = Array.new
	
	# Initialize the problem_files.txt file
	@@problem_files_file = File.join(@@home_dir, "problem_files.txt")
	
	# This is the public method kicking of the compare process
	def self.compare
		Logger.debug("Begin function compare")
		Logger.output_major_status("Running Compare...")
		
		# Get user input for the source path and target path
		# The target path will be compared to the source path
		source_question = "Source Directory Path (type full path or \"done\" to exit): "
		@@source_path = get_input(source_question)
		Logger.info("source_path = " + @@source_path)
		return if @@source_path == "done"
		
		target_question = "Target Directory Path to compare the Source to (type full path or \"done\" to exit): " 
		@@target_path = get_input(target_question)
		Logger.info("target_path = " + @@target_path)
		return if @@target_path == "done"
		
		# Call scan_path to start generating the report
		Logger.info("Running scan_path from top...")
		scan_path(@@source_path, @@target_path)
		Logger.output_major_status("Completed Compare!")
	end

		
	private # Private methods follow
	
	# This method writes a line to the output file
	def self.output(file, text)
		Logger.debug("Begin function output")

		# Attempt to write the text to the output file
		begin
			Logger.info("Appending text to #{file}:\n#{text}")
			File.open(file, 'a') { |file| file.write("#{text}\n")  }
		rescue => e
			# Log an error if there was an exception writing to the output file
			Logger.error("Unable to append text to #{file}:\n#{text}\n" + e.message)
		end
	end
	
	
	# This method gets input from the user and returns it after some error checking
	def self.get_input(question)	
		Logger.debug("Begin function get_input")

		# Set defaults
		response_path = "done"
		answer = false
		
		# Loop on question to the user
		until answer == true
			puts question
			response = gets
			response = response.strip
			
			Logger.info("User response received: #{response}")
				
			# Check if the response is "done", and exit if true
			if response.downcase == "done"
				Logger.info("User entered \"done\". Exiting ...")
				puts "Thank you come again!"
				answer = true
				return response.downcase
			end

			# Turn the user response into a vald path
			response_path = File.expand_path(response)
			Logger.debug("Path to be evaluated: #{response_path}")

			# Check if the response is a valid path
			if not File.exists?(response_path)
				Logger.info("#{response_path} is not a valid path. Requesting user to re-enter ...")
				puts "#{response_path} is not a valid path.  Try again!"
				next
			end
			
			# Check if the response is a directory
			if not File.directory?(response_path)
				Logger.info("#{response_path} is not a directory. Requesting user to re-enter ...")
				puts "#{response_path} is not a directory.  Try again!"
				next
			end

			# Check if the response is a readable directory
			if not File.readable?(response_path)
				Logger.info("#{response_path} is not readable. Requesting user to re-enter ...")
				puts "#{response_path} is not readable.  Try again!"
				next
			end
			
			answer = true
		end
		
		# The response is a valid, readable directory, return the path
		return response_path
	end
	
	
	# This method gets the list of problem files and stores them in global variable @@skip_items
	def self.get_problem_files(problem_file)
		Logger.debug("Begin function get_problem_files ...")

		# Initiate a counter to count the total # of problem files
		counter = 0

		# Read the problem_file and add
  		begin
  			file = File.new(problem_file, "r")
  			while (line = file.gets)
				line = line.strip
  				@@skip_items << line
  				counter = counter + 1
  			end
  			file.close
  		rescue => e
 			Logger.warning("#{proflem_file} could not be read, there will be no problem files to skip. \n" + e.message + "\n" + e.backtrace.inspect)
  		end
		
		# Return the number of items read from the problem_file
		return counter
	end

	# This method 
	def self.scan_path(source_path, target_path)	
		Logger.debug("Begin function scan_path")
		
		Logger.info("Scanning source_path = #{source_path}...")
		Logger.info("target_path = #{target_path}")
		
		# Grab the name of the file or directory
		basename = File.basename(source_path)
		
		# If the source_path points to a file, check if it already exists on the target_path
		if File.file?(source_path)
			Logger.info("#{basename} is a file! Determining if the file already exists on the target_path ...")

			# Check if the same file exists on the target_path
			if not File.exists?(target_path)
				Logger.info("The file #{basename} does not exist on the target_path. Handling ...")
				handle_file(source_path, target_path, basename)
			else
				Logger.info("The file #{basename} exists on the target_path, no action will be taken.")
			end
		
		# If the source_path is a directory, recursively traverse it
		elsif File.directory?(source_path)
      			Logger.info("#{basename} is a directory. Preparing to loop through each entry in the directory (unless it starts with . or $) ...")
			
			# Get the list of items in the directory, filter out unwanteds and sort
			items = Dir.entries(source_path)
			items.delete_if { |x| x.start_with?(".") }
			items.delete_if { |x| x.start_with?("$") }
			items.sort!
			#items.reverse!

			Logger.info("Looping through items in the #{basename} directory ...")
      
			# Traverse the items in this directory calling scan_path recursively
			items.each do |item|
				new_source_path = File.join(source_path, item)
				new_target_path = File.join(target_path, item)
				scan_path(new_source_path, new_target_path)
			end
		else
			Logger.warn("Could not determine if #{basename} is a file or directory. This item will be skipped.")
		end
	end

	def self.handle_file(source_path, target_path, basename)
		Logger.debug("Begin function handle_file")

		Logger.info("source_path: #{source_path}")
		
		# Populate the @@skip_items array with file paths from the problem_file.txt
		Logger.info("Reading problems file: #{@@problem_files_file} ...")
		@@skip_items.clear
		num_problem_files = get_problem_files(@@problem_files_file)
		Logger.info("#{num_problem_files} problem files identified from #{@@problem_files_file}")
		
		# Check if the source_path is on @@skip_items
		if @@skip_items.include?(source_path)
			Logger.info("Skipping #{basename}, it has been identified as a problem file in #{@@problem_files_file}")
			return
		end
		
		Logger.info("Attempting to copy #{basename} to #{target_path} ...")
		
		# Set a timeout so the copy action is interrupted if taking more than @@max_time
		begin
			# Attempt to copy from source_path to target_path
			copy_result = Timeout.timeout(@@max_time) do
				FileUtils.cp(source_path,target_path,:preserve=>true)
			end
		rescue Timeout::Error
			Logger.warn("Did not copy file from #{source_path}, #{@@max_time} seconds was exceeded. Appending output here: #{@@problem_files_file}")
			output(@@problem_files_file, source_path)
		rescue => e
			Logger.error("Unable to copy file from #{source_path}. Appending output here: #{@@problem_files_file}\n" + e.message + "\n" + e.backtrace.inspect)
			output(@@problem_files_file, source_path)
		else
			Logger.info("Successfully copied #{source_path} to #{target_path}")
		end
	end
end

